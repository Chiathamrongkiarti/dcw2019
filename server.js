var app = require('net')
var HOST = '127.0.0.1'
var PORT = 6969
var scores = []
var names = []

app.createServer((sock) => {
    console.log('Connected: ' + sock.remoteAddress + ':' + sock.remotePort)
    var isReadyToCount = false
    var client
    sock.on('data', data => {
        if (!isReadyToCount) {
            console.log("Data receive " + data)
            if (!data.toString().substring(0, 4).localeCompare("I'm ")) {
                client = findClient(data)
                sock.write('Your score is ' + scores[client.clientIdx])
                isReadyToCount = true
            }
            else {
                sock.write('Are You crazy?')
                sock.destroy()
            }
        }
        else{
            if(!data.toString().substring(0, 1).localeCompare('+')){
                scores[client.clientIdx] += parseInt(data.toString().substring(1, data.toString().length))
                sock.write('Your score is ' + scores[client.clientIdx])
            }
            else if(!data.toString().substring(0, 1).localeCompare('-')){
                scores[client.clientIdx] -= parseInt(data.toString().substring(1, data.toString().length))
                sock.write('Your score is ' + scores[client.clientIdx])
            }
            else if(!data.toString().substring(0, 3).localeCompare('BYE')){
                sock.write('Disconnected')
                sock.destroy()
            }
            else{
                sock.write('You should enter => +<score> or -<score>.')
            }
            
            
        }
    })
}).listen(PORT, HOST);
console.log('Server listening on ' + HOST + ':' + PORT);

function findClient(data) {
    var clientName = data.toString().substring(4, data.toString().length)
    var clientIdx = 0
    var clientScore = 0
    for (var i = 0; i < names.length; i++) {
        if (!clientName.localeCompare(names[i])) {
            clientIdx = i
            clientScore = scores[clientIdx]
            return { clientName, clientIdx, clientScore }
        }
    }
    clientIdx = i
    names.push(clientName)
    scores.push(clientScore)
    return { clientName, clientIdx, clientScore }
}