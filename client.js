var net = require('net');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var HOST = '127.0.0.1';
var PORT = 6969;
var client = new net.Socket();

client.connect(PORT, HOST, function () {
    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    rl.question('You send:', (answer) => {
        client.write(answer);
    });
});

client.on('data', function (data) {
    console.log('DATA: ' + data);
    rl.question('', (answer) => {
        client.write(answer);
    });
});

client.on('close', function () {
    console.log('Connection closed');
    process.exit()
});
